context('Login', () => {
  beforeEach(() => {
    cy.visit('/login');
  });

  it('login - bad email format', () => {
    cy.get('input[name=email]').type('adminesgi.fr');
    cy.get('input[name=password]').type('admin{enter}');

    cy.get('p').contains('Must be a valid email');
  });

  it('login - blank password', () => {
    cy.get('input[name=email]').type('admin@esgi.fr');

    cy.get('input[name=password]').type('{enter}');

    cy.get('p').contains('Password is required');

    // test if affiche message
  });

  it('login - bad login account ', () => {
    cy.get('input[name=email]').type('adminss@esgi.fr');
    cy.get('input[name=password]').type('admin{enter}');

    cy.get('p').contains('Please check your email and password');
  });

  it('login - good login account', () => {
    cy.get('input[name=email]').type('admin@esgi.fr');
    cy.get('input[name=password]').type('admin{enter}');

    cy.url().should('include', '/app/reports/dashboard');

    cy.get('.MuiPaper-root > .MuiToolbar-root > .MuiBox-root > .MuiButtonBase-root > .MuiTypography-root').click()
 
    cy.get('body > .MuiPopover-root > .MuiPaper-root > .MuiList-root > .MuiButtonBase-root:nth-child(3)').click()

    cy.url().should('include', '/home');
  });
});
