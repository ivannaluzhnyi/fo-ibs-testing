context('Users', () => {

    beforeEach(() => {
      cy.visit('/login');

      cy.get('input[name=email]').type('admin@esgi.fr');
  
      cy.get('input[name=password]').type('admin{enter}');
      
      cy.url().should('include', '/app/reports/dashboard');
    
      

    });

    it('Users - check all input edit', () => {
    cy.visit('/app/management/customers/1/edit');

      cy.get('form').find('[name="verified"]')

      cy.get('form').find('[name="email"]')
      cy.get('form').find('[name="phone"]')
      cy.get('form').find('[name="country"]')
      cy.get('form').find('[name="address2"]')

      cy.get('form').find('[name="fullName"]')
      cy.get('form').find('[name="state"]')
      cy.get('form').find('[name="address1"]')
      cy.get('form').find('[name="verified"]').should('be.checked')
      cy.get('form').find('[name="discountedPrices"]').should('be.not.checked')
      });

      it('Users - Search user', () => {
        cy.visit('/app/management/customers');
 
    
     
        cy.get('.MuiPaper-root > .MuiBox-root > .MuiFormControl-root:nth-child(1) > .MuiInputBase-root > .MuiInputBase-input').click()
     
        cy.get('.MuiPaper-root > .MuiBox-root > .MuiFormControl-root:nth-child(1) > .MuiInputBase-root > .MuiInputBase-input').type('Ekaterina Tankova')
     
        cy.get('.MuiTableRow-root > .MuiTableCell-root > .MuiButtonBase-root:nth-child(2) > .MuiIconButton-label > .MuiSvgIcon-root').click()

        cy.get('.MuiContainer-root > .MuiGrid-root > .MuiGrid-root > .MuiTypography-h3').contains(
            'Ekaterina Tankova'
          );

        cy.get('.MuiContainer-root > .MuiGrid-root > .MuiGrid-root > .MuiButtonBase-root > .MuiButton-label').click()

        cy.get('form').find('[name="fullName"]').should('have.value', 'Ekaterina Tankova');
        cy.get('form').submit();
        cy.get('.MuiSnackbarContent-root').contains('Customer updated');
        });

  });
  // 