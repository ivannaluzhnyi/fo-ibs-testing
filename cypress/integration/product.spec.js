context('Products', () => {

    beforeEach(() => {
      cy.visit('/login');

      cy.get('input[name=email]').type('admin@esgi.fr');
  
      cy.get('input[name=password]').type('admin{enter}');
      
      cy.url().should('include', '/app/reports/dashboard');
    
      cy.visit('/app/management/products/create');

    });

    it('product - check all input', () => {
   
      cy.get('form').find('[name="name"]')

      cy.get('form').find('[name="price"]')
      cy.get('form').find('[name="salePrice"]')
      cy.get('form').find('[name="isTaxable"]')
      cy.get('form').find('[name="includesTaxes"]')

      cy.get('form').find('[name="category"]')
      cy.get('form').find('[name="productCode"]')
      cy.get('form').find('[name="productSku"]')
      });


   it('Products - forget and put value inside input', () => {
   
    cy.get('form').submit();
    cy.get('p').contains('name is a required field')
    cy.get('.MuiPaper-root > .MuiCardContent-root:nth-child(1) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('Tente 5 personnes{enter}')
    cy.get('form').submit();
    cy.get('p').contains('price is a required field')

    cy.get('.MuiGrid-root > .MuiGrid-root:nth-child(1) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('50')
    cy.get('form').submit();

    cy.url().should('include', '/app/management/products');
    cy.get('.MuiSnackbarContent-root').contains('Product Created');
    });
  });

  // 