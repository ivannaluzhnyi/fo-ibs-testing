context('Chat', () => {
  beforeEach(() => {
    cy.visit('/login');

    cy.get('input[name=email]').type('admin@esgi.fr');
    cy.get('input[name=password]').type('admin{enter}');
  });

  it('Chat - initial send btn is disabled', () => {
    cy.visit('/app/chat/new');

    cy.get('.send-message-btn').should('be.disabled');
  });

  it('Chat - should display options list', () => {
    cy.visit('http://localhost:3000/app/chat/new');
    cy.get('.makeStyles-input-38 > .MuiInputBase-input').click();
    cy.get('.makeStyles-input-38 > .MuiInputBase-input').type('alex');
    cy.get('.MuiPaper-root > .MuiList-root .MuiTypography-root').click();
    cy.get(
      '.makeStyles-root-61 > .MuiButtonBase-root .MuiSvgIcon-root'
    ).click();

    cy.get('.menu-chat-options')
      .children()
      .should('have.length', 4);

    cy.get('.MuiMenuItem-root:nth-child(1) .MuiTypography-root').contains(
      'Block contact'
    );
    cy.get('.MuiMenuItem-root:nth-child(2) .MuiTypography-root').contains(
      'Delete thread'
    );

    cy.get('.MuiMenuItem-root:nth-child(3) .MuiTypography-root').contains(
      'Archive thread'
    );

    cy.get('.MuiMenuItem-root:nth-child(4) .MuiTypography-root').contains(
      'Mute notifications'
    );
  });

  it('Chat - send message', () => {
    cy.get(
      '.MuiToolbar-root > .MuiButtonBase-root:nth-child(1) .MuiSvgIcon-root'
    ).click();
    cy.get(
      '.MuiListItem-root:nth-child(3) > .depth-0 > .MuiButton-label'
    ).click();
    cy.get('.makeStyles-input-135 > .MuiInputBase-input').click();
    cy.get('.makeStyles-input-135 > .MuiInputBase-input').type('alex');
    cy.get('.MuiBox-root > .MuiList-root > .MuiButtonBase-root').click();
    cy.get('.MuiInputBase-fullWidth > .MuiInputBase-input').click();
    cy.get('.MuiInputBase-fullWidth > .MuiInputBase-input').type(
      'Salut! Ceci un test de cypress!!!!'
    );
    cy.get('.MuiIconButton-colorSecondary').click();
    cy.get('.MuiBox-root-183').contains('Salut! Ceci un test de cypress!!!!');
    cy.get('.contacts-list')
      .children()
      .should('have.length', 3);
  });
});
