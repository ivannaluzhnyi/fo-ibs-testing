context('Register', () => {
  beforeEach(() => {
    cy.visit('/register');
  });

  it('Register - wrong password', () => {
    cy.get('input[name=email]').type('ivan.naluzhnyi@gmail.com');
    cy.get('input[name=password]').type('test12');

    cy.get('input[name=firstName]').type('Ivan');
    cy.get('input[name=lastName]').type('Naluzhnyi');

    cy.get('input[name=policy]').check();
    cy.get('p').contains('password must be at least 7 characters');
  });

  it('Register - checkbox is not checked', () => {
    cy.get('input[name=email]').type('ivan.naluzhnyi@gmail.com');
    cy.get('input[name=password]').type('test123');
    cy.get('input[name=firstName]').type('Ivan');
    cy.get('input[name=lastName]').type('Naluzhnyi{enter}');
    cy.get('p').contains('This field must be checked');
  });

  it('Register', () => {
    cy.get('input[name=email]').type('ivan.naluzhnyi@gmail.com');
    cy.get('input[name=password]').type('test123');
    cy.get('input[name=firstName]').type('Ivan');
    cy.get('input[name=policy]').check();
    cy.get('input[name=lastName]').type('Naluzhnyi{enter}');
    cy.url().should('include', '/login');

    cy.get('.MuiSnackbarContent-root').contains('User created');
  });
});
